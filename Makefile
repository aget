all:
	@gnatmake -p -Paget.gpr

clean:
	@rm -rf obj/*

distclean: clean
	@rm -rf obj

.PHONY: clean
