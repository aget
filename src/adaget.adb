with Ada.Directories;
with Ada.Text_IO;
with Ada.Streams.Stream_IO;

with AWS.Response;

package body AdaGet is

   procedure Create_Or_Open_File
     (Filename :     String;
      File     : out Ada.Streams.Stream_IO.File_Type;
      Filesize : out AWS.Client.Content_Bound);
   --  Create file with given filename. If the file already exists it is opened
   --  instead and the filesize is returned. Filesize ist set to zero if the
   --  file has been created.

   -------------------------------------------------------------------------

   procedure Create_Or_Open_File
     (Filename :     String;
      File     : out Ada.Streams.Stream_IO.File_Type;
      Filesize : out AWS.Client.Content_Bound)
   is
   begin
      if Ada.Directories.Exists (Name => Filename) then
         Filesize := AWS.Client.Content_Bound
           (Ada.Directories.Size (Name => Filename));
         Ada.Streams.Stream_IO.Open
           (File => File,
            Mode => Ada.Streams.Stream_IO.Append_File,
            Name => Filename);
      else
         Filesize := 0;
         Ada.Streams.Stream_IO.Create
           (File => File,
            Mode => Ada.Streams.Stream_IO.Out_File,
            Name => Filename);
      end if;

   end Create_Or_Open_File;

   -------------------------------------------------------------------------

   procedure Download_File
     (Connection : in out AWS.Client.HTTP_Connection;
      URI        :        String;
      Filename   :        String;
      Size       :        AWS.Client.Content_Bound;
      Chunk_Size :        AWS.Client.Content_Bound)
   is
      use type AWS.Client.Content_Bound;

      Cur_Start   : AWS.Client.Content_Bound := AWS.Client.Content_Bound'First;
      Cur_End     : AWS.Client.Content_Bound := AWS.Client.Content_Bound'First;
      Local_File  : Ada.Streams.Stream_IO.File_Type;
      Data        : AWS.Response.Data;
      One_Percent : constant AWS.Client.Content_Bound := Size / 100;
   begin
      AdaGet.Create_Or_Open_File
        (Filename => Filename,
         File     => Local_File,
         Filesize => Cur_End);

      --  Cause end is set to filesize it is off by one.

      Cur_End := Cur_End - 1;

      Ada.Text_IO.Put_Line ("Chunk size :" & Chunk_Size'Img);
      Ada.Text_IO.Put_Line ("Saving to  : " & Filename);

      while Cur_End < Size loop
         Ada.Text_IO.Put ("...");
         Cur_Start := Cur_End + 1;
         Cur_End   := AWS.Client.Content_Bound'Min
           (Cur_Start + Chunk_Size, Size);

         AWS.Client.Get (Connection => Connection,
                         Result     => Data,
                         URI        => URI & Filename,
                         Data_Range => (First => Cur_Start,
                                        Last  => Cur_End));
         Ada.Streams.Stream_IO.Write
           (File => Local_File,
            Item => AWS.Response.Message_Body (D => Data));

         Ada.Text_IO.Put (AWS.Client.Content_Bound (Cur_End / One_Percent)'Img
                          & "%");
      end loop;
      Ada.Text_IO.New_Line;

      Ada.Streams.Stream_IO.Close (File => Local_File);

   exception
      when others =>
         Ada.Streams.Stream_IO.Close (File => Local_File);
         AWS.Client.Close (Connection => Connection);
         raise;
   end Download_File;

end AdaGet;
