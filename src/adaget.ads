with AWS.Client;

package AdaGet is

   procedure Download_File
     (Connection : in out AWS.Client.HTTP_Connection;
      URI        :        String;
      Filename   :        String;
      Size       :        AWS.Client.Content_Bound;
      Chunk_Size :        AWS.Client.Content_Bound);
   --  Download remote resource (URI + Filename) using given initialized
   --  connection and store it to a file specified by filename. Size specifies
   --  the content size of the online resource. the chunk size parameter is
   --  used to split up the download in multiple chunks of given size.

end AdaGet;
