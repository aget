with Ada.Text_IO;
with Ada.Exceptions;
with Ada.Command_Line;

with AWS.Client;
with AWS.Response;
with AWS.URL;

with AdaGet;

procedure Aget is

   procedure Print_Usage;
   --  Print usage information for aget.

   procedure Print_Usage
   is
   begin
      Ada.Text_IO.Put_Line
        ("Usage: " & Ada.Command_Line.Command_Name
         & " <URL to download>");
   end Print_Usage;

   Con  : AWS.Client.HTTP_Connection;
   Data : AWS.Response.Data;
begin
   if Ada.Command_Line.Argument_Count /= 1 then
      Print_Usage;
      Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Success);
      return;
   end if;

   declare
      URL_String : constant String := Ada.Command_Line.Argument (Number => 1);
      URL        : constant AWS.URL.Object := AWS.URL.Parse
        (URL => URL_String);
      Protocol   : constant String := AWS.URL.Protocol_Name (URL);
      Server     : constant String := AWS.URL.Host (URL);
      URI        : constant String := AWS.URL.Path (URL);
      Filename   : constant String := AWS.URL.File (URL);
   begin
      if not AWS.URL.Is_Valid (URL) or else Filename'Length = 0 then
         Ada.Text_IO.Put_Line ("Invalid URL specified");
         Print_Usage;
         Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Failure);
         return;
      end if;

      AWS.Client.Create (Connection  => Con,
                         Host        => Protocol & "://" & Server,
                         User_Agent  => "aget");
      AWS.Client.Head (Connection => Con,
                       Result     => Data,
                       URI        => URI & Filename);

      Ada.Text_IO.Put_Line ("Getting resource " & Server & URI & Filename);
      Ada.Text_IO.Put_Line
        ("Type       : " & AWS.Response.Content_Type (D => Data));

      declare
         use type AWS.Client.Content_Bound;

         Size       : constant AWS.Client.Content_Bound :=
           AWS.Client.Content_Bound (AWS.Response.Content_Length (D => Data));
         Chunk_Size : constant AWS.Client.Content_Bound := Size / 10;
      begin
         Ada.Text_IO.Put_Line ("Size       :" & Size'Img);

         AdaGet.Download_File
           (Connection => Con,
            URI        => URI,
            Filename   => Filename,
            Size       => Size,
            Chunk_Size => Chunk_Size);
         AWS.Client.Close (Connection => Con);

      exception
         when E : others =>
            AWS.Client.Close (Connection => Con);
            Ada.Text_IO.Put_Line
              ("Download error - " & Ada.Exceptions.Exception_Name (X => E));
            Ada.Text_IO.Put_Line (Ada.Exceptions.Exception_Message (X => E));
            Ada.Command_Line.Set_Exit_Status
              (Code => Ada.Command_Line.Failure);
            return;
      end;
   end;

   Ada.Text_IO.Put_Line ("Done");
   Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Success);

exception
   when E : others =>
      AWS.Client.Close (Connection => Con);
      Ada.Text_IO.Put_Line ("Caugth unhandled exception");
      Ada.Text_IO.Put_Line (Ada.Exceptions.Exception_Information (X => E));
      Ada.Command_Line.Set_Exit_Status (Code => Ada.Command_Line.Failure);
end Aget;
